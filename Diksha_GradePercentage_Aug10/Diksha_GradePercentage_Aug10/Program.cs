﻿using System;

namespace Diksha_GradePercentage_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            int s1, s2, s3, s4, s5;
            double per;
            Console.WriteLine("enter marks of 5 subjects");
            s1 = Convert.ToInt32(Console.ReadLine());
            s2 = Convert.ToInt32(Console.ReadLine());
            s3 = Convert.ToInt32(Console.ReadLine());
            s4 = Convert.ToInt32(Console.ReadLine());
            s5 = Convert.ToInt32(Console.ReadLine());

            per = ((s1 + s2 + s3 + s4 + s5) / 5.0);

            if (per >= 90)
            {
                Console.WriteLine("Grade A");
            }
            else if((per<90) && (per>=80))
            {
                Console.WriteLine("Grade B");
            }
            else if((per<80)&& (per >= 65))
            {
                Console.WriteLine("Grade C");
            }
            else
            {
                Console.WriteLine("Fail");
            }
        }
    }
}