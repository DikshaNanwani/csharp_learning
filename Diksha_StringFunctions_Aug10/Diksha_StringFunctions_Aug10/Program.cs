﻿using System;

namespace Diksha_StringFunctions_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            
            String str = "allanayat dghjkkoiytfd   ";

            Console.WriteLine("Remove Method");
             String str1=str.Remove(0,1);
            Console.WriteLine(str1);

            Console.WriteLine("\nReplace Method");
            String str2 = str.Replace("a", "d");
            Console.WriteLine(str2);

            Console.WriteLine("\nUpper String");
            String str3 = str.ToUpper();
            Console.WriteLine(str3);

           Console.WriteLine("\nleft Trim");
            String str4 = str.TrimEnd();
           Console.WriteLine(str4);

            Console.WriteLine("\nlast Index");
            int i = str.LastIndexOf("a");
            Console.WriteLine(i);

            Console.WriteLine("\n containing");
            Boolean j = str.Contains("a");
            Console.WriteLine(j);

            //Console.WriteLine("\nstart with");
            //string str7 = str.StartsWith("a");
            //Console.WriteLine(str7);
        }
    }
}
