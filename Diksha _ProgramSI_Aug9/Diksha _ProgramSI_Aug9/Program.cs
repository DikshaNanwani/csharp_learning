﻿using System;

namespace Diksha__ProgramSI_Aug9
{
    class Program
    {
        static void Main(string[] args)
        {
            double P,R,T;
            Console.WriteLine("Enter the principal value");
             P = Convert.ToDouble(Console.ReadLine());
             Console.WriteLine("Enter the Interest Rate value");
             R = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter the time value in years");
            T = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Simple Interest=");
            Console.WriteLine((P*R*T)/100);
        }
    }
}
