﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Diksha_CQRS_Aug29.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IMediator _mediator;


        public StudentController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/<StudentController>
        [HttpGet]
        public async Task<List<Student>> Get()
        {
            return await _mediator.Send(new GetStudentListQuery()); ;
        }

        // GET api/<StudentController>/5
        [HttpGet("{id}")]
        public async Task<Student> Get(int id)
        {
            return await _mediator.Send(new GetStudentByIdQuery(id));
        }

        // POST api/<StudentController>
        [HttpPost]
        public async Task<<Student> Post([FromBody] Student value)
        {
            var model = new AddStudentCommand(value.FirstName, value.LastName, value.Age);
            return await _mediator.Send(model);
        }

        //    // PUT api/<StudentController>/5
        //    [HttpPut("{id}")]
        //    public void Put(int id, [FromBody] string value)
        //    {
        //    }

        //    // DELETE api/<StudentController>/5
        //    [HttpDelete("{id}")]
        //    public void Delete(int id)
        //    {
        //    }
        }
    }

