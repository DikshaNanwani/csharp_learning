﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS_Library.Models
{
   public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double Age { get; set; }

    }
}
