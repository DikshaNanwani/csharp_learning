﻿using System;
using System.IO;


namespace Diksha_HW_FileHandling1_writeinfile
{
    class Program
    {
        class WriteinFile
        {
            public void info()
            {
                StreamWriter sw = new StreamWriter("E:\\Homework_1.txt");
                Console.WriteLine("Enter the text you want to write in a file");
                string str = Console.ReadLine();
                sw.WriteLine(str);
                sw.Flush();
                sw.Close();
            }
        }
        static void Main(string[] args)
        {
            WriteinFile wf = new WriteinFile();
            wf.info();
            Console.ReadKey();
        }
    }
}
