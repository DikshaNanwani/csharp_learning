﻿using System;
using System.Collections.Generic;

namespace Diksha_HW_Generics1
{
    public class mygenericarray<T>
    {
        private T[] array;

        public mygenericarray(int size)
        {
            array = new T[size + 1];
        }

        public T getItem(int index)
        {
            return array[index];
        }
        public void setItem(int index, T value)
        {
            array[index] = value;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            mygenericarray<char> intarray = new mygenericarray<char>(5);

            for (int d = 0; d < 5; d++)
            {
                intarray.setItem(d, (char)(d+97));
            }

            for (int d = 0; d < 5; d++)
            {
                Console.Write(intarray.getItem(d) + " ");
            }

            Console.WriteLine();
            Console.ReadKey();
        }
    }
}

