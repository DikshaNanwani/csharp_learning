﻿using System;

namespace Diksha_VehicleCar_Aug11
{
    class vehicle
    {
        public string vehicleid;
        public string vehiclename;
        public string vehiclecolour;

       
    }

    class car : vehicle
    {
        public void showdetails()
        {
            Console.WriteLine("\nDetails of vehicle:");
            Console.WriteLine("car Id:" + vehicleid);
            Console.WriteLine("car name:" + vehiclename);
            Console.WriteLine("car Colour:" + vehiclecolour);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            car car1 = new car();
            Console.Write("enter car id:");
            car1.vehicleid = Console.ReadLine();
            Console.Write("enter car name:");
            car1.vehiclename = Console.ReadLine();
            Console.Write("enter car colour:");
            car1.vehiclecolour = Console.ReadLine();

            car1.showdetails();

        }
    }
}
