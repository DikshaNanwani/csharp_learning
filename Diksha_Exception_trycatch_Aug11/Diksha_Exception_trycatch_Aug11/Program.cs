﻿using System;

namespace Diksha_Exception_trycatch_Aug11
{
    class Program
    {
        static void Main(string[] args)

        {
            int[] arr = new int[3] { 3, 4, 5 };
            try
            {
                Console.WriteLine(arr[8]);
                

            }
            catch(IndexOutOfRangeException ior)
            {
                Console.WriteLine("Wrong msg");
                Console.WriteLine(ior.Message);

            }
        }
    }
}
 