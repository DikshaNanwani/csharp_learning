﻿using System;
using System.Collections;

namespace Diksha_ExampleEnumerator_Aug11
{
    class Program
    {
        private void btnArray_Click(object sender, EventArgs e)
        {
            ;
            char[] arr = new char[] { 'd','i','k'};
            

            IEnumerator iEnum = arr.GetEnumerator();

            string msg = " ";
            while (iEnum.MoveNext())
            {
                msg += iEnum.Current.ToString() + "\n";
            }

            Console.WriteLine(msg, "GetEnumerator() on ArrayList");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
    
}
