﻿using System;

namespace Diksha_Theatremovies_aug11
{
    class Program
    {
        abstract class theatre
        {
            abstract public void showmovies();
        }

        class movies : theatre
        {
            int noofshows;
            string latestmovie;

            public override void showmovies()
            {
                Console.WriteLine("No of shows:"+ noofshows);
                Console.WriteLine("latest movie" + latestmovie);

            }
        }
        static void Main(string[] args)
        {
            movies m1 = new movies();
            Console.WriteLine("Enter Details");
            m1.noofshows = Console.ReadLine();
            m1.latestmovie = Convert.ToInt32(Console.ReadLine());
            m1.showmovies();
        }
    }
}
