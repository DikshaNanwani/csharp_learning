﻿using System;

namespace Diksha_GenericsExample_12Aug
{
    public class SomeClass
    {
        public void GenericMethod<T>(T Param1, T Param2)
        {
            Console.WriteLine($"Parameter 1:{Param1}: Parameter 2:{Param2}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generics Method Example");
            SomeClass s = new SomeClass();
            //While calling we need to specify the type
            s.GenericMethod<int>(10, 20);
            s.GenericMethod(10.3, 20.5);
            s.GenericMethod("hi", "there");
            Console.ReadLine();
        }
    }
}
