﻿using System;

namespace Diksha_AbstractExample_Aug11
{
    class Program
    {
        abstract class areaclass
        {
            abstract public int Area();
        }

        class Square : areaclass
        {
            int side = 0;
            public Square(int n)
            {
                side = n;
            }

            public override int Area()
            {
                return side * side;
            }
        }
        static void Main(string[] args)
        {
            Square s = new Square(6);
            Console.WriteLine("Area=:" + s.Area());
        }
    }
}
