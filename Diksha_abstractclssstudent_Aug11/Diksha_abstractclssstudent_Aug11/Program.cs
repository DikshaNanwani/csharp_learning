﻿using System;

namespace Diksha_abstractclssstudent_Aug11
{
    class Program
    {
        abstract class Class
        {
            public string studentrollno;
            abstract public void show();
        }

        class student : Class
        {
            public override void show()
            {
                Console.WriteLine("Student Roll no:" + studentrollno);
            }
        }
        static void Main(string[] args)
        {
            student s1 = new student();
            Console.WriteLine("enter student rollno:");
            s1.studentrollno = Console.ReadLine();
            s1.show();
        }
    }
}
