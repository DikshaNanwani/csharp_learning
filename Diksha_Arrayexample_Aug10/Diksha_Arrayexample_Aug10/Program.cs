﻿using System;

namespace Diksha_Arrayexample_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3, 4, 5 };

            foreach(var item in arr)
            {
                Console.WriteLine(item);
            }
           // int[] var = new int[5];
        }
    }
}
