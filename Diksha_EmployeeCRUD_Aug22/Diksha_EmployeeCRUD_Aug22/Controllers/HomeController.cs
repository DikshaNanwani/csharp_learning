﻿using Diksha_EmployeeCRUD_Aug22.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Diksha_EmployeeCRUD_Aug22.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        private static List<Employee> Employee = new List<Employee>()
        {
            new Employee{empid=Guid.NewGuid(),empname="Mansi",doj=new DateTime(2011,01,09),role="Manager"},
             new Employee{empid=Guid.NewGuid(),empname="Khushi",doj=new DateTime(2020,09,11),role="Senior-Analyst"},
        };
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
       
        public ActionResult AddEmployee(Employee input)
        {
            Employee.Add(new Employee()
            { 
                empid = Guid.NewGuid(), 
                empname = input.empname, 
                doj = input.doj, 
                role = input.role 
            });
            return RedirectToAction("EmployeeList");
        }

        public ActionResult EmployeeList()
        {
            ViewBag.empList = Employee;
            return View();
        }

        public ActionResult DeleteEmployee(Guid empid)
        {
            Employee.Remove(Employee.SingleOrDefault(o => o.empid == empid));

        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
