﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diksha_EmployeeCRUD_Aug22.Models
{
    public class Employee
    {
        public Guid empid{ get; set; }
        public string empname { get; set; }
        public DateTime doj { get; set; }
        public string role { get; set; }

    }
}
