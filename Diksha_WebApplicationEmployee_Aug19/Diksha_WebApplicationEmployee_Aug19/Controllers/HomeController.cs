﻿using System.Diagnostics;
using Diksha_WebApplicationEmployee_Aug19.Models;
using Microsoft.AspNetCore.Mvc;

namespace Diksha_WebApplicationEmployee_Aug19.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        
        public ViewResult Details()
        {
            Employee emp = new Employee()
            {
                EmpId = 1,
                EmpName = "Diksha",

                
        };
            //Guid gid = Guid.NewGuid();
            //emp.id = gid;
            ViewData["Employee"]=emp;   
            return View(emp);   
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}