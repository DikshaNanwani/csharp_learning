﻿using System;

namespace Diksha_DIExample_Aug19.Controllers
{
    public interface IDateTime
    {
        object Now { get; }

        public static implicit operator DateTime(IDateTime v);
    }
}