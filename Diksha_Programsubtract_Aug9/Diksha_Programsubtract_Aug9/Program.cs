﻿using System;

namespace Diksha_Programsubtract_Aug9
{
    class Program
    {
        static void Main(string[] args)
        {
            int Fvalue, Svalue;
            Console.WriteLine("Enter the first value");
            Fvalue = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Second value");
            Svalue = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("First Value-Second Value=");
            Console.WriteLine(Fvalue - Svalue);
        }
    }
}
