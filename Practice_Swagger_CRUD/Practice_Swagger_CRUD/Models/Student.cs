﻿namespace Practice_Swagger_CRUD.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }

        public int StudentAge { get; set; }
    }
}
