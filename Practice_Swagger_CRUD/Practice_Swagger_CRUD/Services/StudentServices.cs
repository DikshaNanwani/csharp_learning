﻿using Practice_Swagger_CRUD.Interface;
using Practice_Swagger_CRUD.Models;
using System.Collections.Generic;
using System.Linq;

namespace Practice_Swagger_CRUD.Services
{
    public class StudentServices : StudentInterface
    {
        public List<Student> stu = new List<Student>()
        {
            new Student(){StudentId=1,StudentName="Diksha",StudentAge=21},
            new Student(){StudentId=2,StudentName="Dik",StudentAge=22}
        };
        
       
      

        public List<Student> AddStudent(Student student)
        {
           stu.Add(student);
            return stu;
        }

        public List<Student> GetAllStudents()
        {
            return stu;
        }

        public Student GetStudent(int id)
        {
            return stu.SingleOrDefault(c => c.StudentId == id);
        }
    }
}
