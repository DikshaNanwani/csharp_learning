﻿using Practice_Swagger_CRUD.Models;
using System.Collections.Generic;

namespace Practice_Swagger_CRUD.Interface
{
    public interface StudentInterface
    {
        List<Student> AddStudent(Student student);

        List<Student> GetAllStudents();

        Student GetStudent(int id);
    }
}
