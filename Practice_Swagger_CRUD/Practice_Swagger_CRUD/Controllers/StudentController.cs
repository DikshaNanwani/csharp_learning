﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Practice_Swagger_CRUD.Interface;
using Practice_Swagger_CRUD.Models;
using System.Collections.Generic;

namespace Practice_Swagger_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly StudentInterface _student;

        public StudentController(StudentInterface student)
        {
            _student = student;
        }

        [HttpPost]
        public List<Student> Post(Student student) { 
            return _student.AddStudent(student);
        }

        [HttpGet]
        public List<Student> GetAllStudents()
        {
            return _student.GetAllStudents();
        }

        [HttpGet("{id}")]
        public Student GetStudent(int id)
        {
            return _student.GetStudent(id);
        }

    }
}