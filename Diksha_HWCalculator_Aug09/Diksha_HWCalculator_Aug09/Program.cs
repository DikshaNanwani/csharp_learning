﻿using System;

namespace Diksha_HWCalculator_Aug09
{
    class Program
    {
        static void Calculator(int num1,int num2)
        {
            Console.WriteLine("Addition is:" + (num1 + num2));
            Console.WriteLine("Substraction is:" + (num1 - num2));
            Console.WriteLine("Multiplication  is:" + (num1 * num2));
            Console.WriteLine("Division is:" + (num1 / num2));
        }
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            int first, second;
            Console.WriteLine("enter first and second value:");
            first = Convert.ToInt32(Console.ReadLine());
            second = Convert.ToInt32(Console.ReadLine());
            Calculator(first, second);
        }
    }
}
