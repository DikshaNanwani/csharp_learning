﻿using System;
using System.Collections;

namespace Diksha_Idictionaryhash_Aug12
{
    class Program
    {
       
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            ht.Add(1, "diksha");
            ht.Add(2, "Sharon");
            ht.Add(3, "sdfgyhuji");

            foreach(DictionaryEntry entry in ht)
            {
                Console.WriteLine(entry.Key.ToString() + " :  " + entry.Value.ToString());
            }

            Console.WriteLine(ht.Contains(1).ToString());

        }
    }
}
