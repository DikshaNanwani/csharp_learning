﻿using System;

namespace Diksha__LinqExample_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myarr= { 1, 2, 47, 89, 90 };
            var dfg = Array.Find(myarr, myarr => myarr == 89);
            Console.WriteLine(dfg);

        }
    }
}
