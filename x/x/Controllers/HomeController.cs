﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using x.Models;

namespace x.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            ViewData["abc"] = "Hi there";
            ViewBag.someexample = "This is other side";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public ViewResult Info()
        {
            Student student = new Student() {
                StudentId = 1,
                 StudentName = "Diksha",
        };
            ViewData["Student"] = student;
               return View();
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
