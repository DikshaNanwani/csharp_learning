﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CQRSPractice_Library.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double Age { get; set; }
    }
}
