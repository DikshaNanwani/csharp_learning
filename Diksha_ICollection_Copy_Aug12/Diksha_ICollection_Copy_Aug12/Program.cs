﻿using System;
using System.Collections;

namespace Diksha_ICollection_Copy_Aug12
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] source = new int[] { 1, 2, 3, 4, 5 };
            int[] destination = new int[source.Length];

            source.CopyTo(destination, 0);
            foreach (int i in destination)
            {
                Console.WriteLine(i);

                ArrayList source1 = new ArrayList();
                source1.Add(123);
                source1.Add("Diksha");
                source1.Add("23.9");

                ArrayList destination1 = new ArrayList();
                destination1[0] = 1;
                destination1[1] = 2;

                source1.CopyTo(destination1, 2);
                foreach (int j in destination1)
                {
                    Console.WriteLine(j);


                }
            }
        }
    }
}
