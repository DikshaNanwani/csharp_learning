﻿using System;
using System.IO;

namespace Diksha_FileHandling_Aug12
{
    class Program
    {
        class FileWrite
        {
            public void writeData()
            {
                FileStream fs=new FileStream("E:\\Example.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                Console.WriteLine("Enter the Text you want to write in File");
                string str = Console.ReadLine();
                sw.WriteLine(str);
                sw.Flush();
                sw.Close();
                fs.Close();
            }
        }
        static void Main(string[] args)
        {
            FileWrite wr = new FileWrite();
            wr.writeData();
        }
    }
}
