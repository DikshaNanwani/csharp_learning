﻿using System;

namespace Diksha_AbstarctVehicle
{
    class Program
    {
        abstract class vehicle
        {
            public string vehicleid;
            abstract public void showdetails();
        }

        class Car : vehicle
        {
            public override void showdetails()
            {
                Console.WriteLine("Car Id:" + vehicleid);
            }
        }
        static void Main(string[] args)
        {
            Car c1 = new Car();
            Console.WriteLine("enter the vehicle id:");
            c1.vehicleid = Console.ReadLine();
            c1.showdetails();
        }
    }
}
