﻿using System;

namespace Diksha_Inheritance_aug11
{
    class shape
    {
        public double Width;
        public double height;

        public void showDim()
        {
            Console.WriteLine("width and height are:" + Width + "and" + height);
        }
    }

    class Triangle : shape
    {
        public string style;

        public double area()
        {
            return Width * height / 2;
        }

        public void showstyle()
        {
            Console.WriteLine("Triangle is:" + style);
        }

        
    }
    class Program
    {
        static void Main(string[] args)
        {
            Triangle t1 = new Triangle();
            Triangle t2 = new Triangle();
            Console.WriteLine("Enter width value");
            t1.Width = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter height value");
            t1.height = Convert.ToInt32(Console.ReadLine());
            t1.style = "isosceles";
            t2.Width = 8.0;
            t2.height = 12.0;
            t2.style = "right";
            Console.WriteLine("Info for T1:");
            t1.showstyle();
            t1.showDim();
            Console.WriteLine("Area is:" + t1.area());
            Console.WriteLine();
            Console.WriteLine("Info for T2:");
            t2.showstyle();
            t2.showDim();
            Console.WriteLine("Area is:" + t2.area());
        }
    }
}
