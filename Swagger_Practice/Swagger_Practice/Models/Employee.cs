﻿namespace Swagger_Practice.Models
{
    public class Employee
    {
        public int EmpId{ get; set; }
        public string EmpName{ get; set; }

        public int EmpSalary { get; set; }
    }
}
