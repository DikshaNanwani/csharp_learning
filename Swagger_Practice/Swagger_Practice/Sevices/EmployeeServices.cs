﻿using Swagger_Practice.Isevices;
using Swagger_Practice.Models;
using System.Collections.Generic;
using System.Linq;

namespace Swagger_Practice.Sevices
{
    public class EmployeeServices : IEmployeeServices
    {
        public List<Employee> emp = new List<Employee>()
        {
            new Employee(){EmpId = 1,EmpName="Mansi",EmpSalary=30000},
            new Employee(){EmpId = 2,EmpName="Devisha",EmpSalary=40000},
        };

        public List<Employee> AddEmployee(Employee e)
        {
            emp.Add(e);
            return emp;
        }


        public List<Employee> DeleteEmployee(int id)
        {
            emp.RemoveAll(o => o.EmpId == id);
            return emp;

        }

        public Employee GetById(int Eid)
        {
            return emp.SingleOrDefault(o => o.EmpId == Eid);
        }

        public List<Employee> Gets()
        {
            return emp;
        }


        public List<Employee> UpdateEmployee(int id)
        {
            return emp;
        }
    }
}
    
