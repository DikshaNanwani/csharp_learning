﻿using Microsoft.AspNetCore.Mvc;
using Swagger_Practice.Isevices;
using Swagger_Practice.Models;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Swagger_Practice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        IEmployeeServices _Employee;

        public EmployeeController(IEmployeeServices _e) {
            _Employee = _e;
        }

        // GET: api/<EmployeeController>
        [HttpGet]
        public List<Employee> Get()
        {
            return _Employee.Gets();
        }

        // GET api/<EmployeeController>/5
        [HttpGet("{id}")]
        public Employee Get(int id)
        {
            return _Employee.GetById(id);
            
        }

        // POST api/<EmployeeController>
        [HttpPost]
        public List<Employee> Post(Employee empl)
        {
            return _Employee.AddEmployee(empl);
        }

        // PUT api/<EmployeeController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        public List<Employee> Delete(int id)
        {
           return _Employee.DeleteEmployee(id);
            
        }
    }
}
