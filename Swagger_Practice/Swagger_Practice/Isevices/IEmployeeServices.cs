﻿using Swagger_Practice.Models;
using System.Collections.Generic;

namespace Swagger_Practice.Isevices
{
    public interface IEmployeeServices
    {
        public List<Employee> Gets();
        public Employee GetById(int id);

        public List<Employee> AddEmployee(Employee e);

        public List<Employee> DeleteEmployee(int id);

        public List<Employee> UpdateEmployee(int id);
    }
}
