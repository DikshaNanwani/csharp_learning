﻿using System;
using System.IO;

namespace Diksha_HW_Filehandling_read
{
    class Program
    {
        class ReadfromFile
        {
            public void Readdata()
            {
                StreamReader sr = new StreamReader("E:\\Homework_1.txt");
                Console.WriteLine("Content of File");

                sr.BaseStream.Seek(0, SeekOrigin.Begin);
                string str = sr.ReadLine();

                while (str != null)
                {
                    Console.WriteLine(str);
                    str = sr.ReadLine();
                }
                Console.ReadLine();
                sr.Close();
            }
        }
        static void Main(string[] args)
        {
            ReadfromFile rd = new ReadfromFile();
            rd.Readdata();
        }
    }
}
