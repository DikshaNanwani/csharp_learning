﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Diksha_mvcDatabase_Aug22.Models
{
    public partial class EmployeeDetails
    {   [Required]
        public int Empid { get; set; }
        [Required]
        public string Empname { get; set; }
    }
}
