﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diksha_WebApplication_Employee.Models
{
    public class Employee
    {
        public int  EmpId { get; set; }
        public string EmpName { get; set; }

        public Guid id { get; set; }
    }
}
