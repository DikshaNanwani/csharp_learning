﻿using Diksha_WebApplication_Employee.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Diksha_WebApplication_Employee.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        private static IList<Employee> emp = new List<Employee>
        {
          new Employee(){id=Guid.NewGuid(),EmpId=7,EmpName="sdfghj"},
          new Employee(){id = Guid.NewGuid(),EmpId = 8,EmpName = "xcvbnnj"},
         };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public ViewResult Details()
        {
            Employee emp = new Employee()
            {
                EmpId = 1,
                EmpName="Devisha",
            };
            ViewData["Employee"] = emp;
            return View();
        }
        public IActionResult Employeelist()
        {
            return View(emp);
        }
        
        public IActionResult AddEmployee()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddEmployee(Employee input)
        {
            emp.Add(new Employee { id = Guid.NewGuid(), EmpId =input.EmpId, EmpName = input.EmpName });
            return RedirectToAction("Employeelist");
        }

        //public IActionResult Delete(Guid id)
        //{
        //    emp.Remove(emp.SingleOrDefault(a=> a.EmpId == id));
        //    return RedirectToAction("Details");

        //}
        //[HttpGet]

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
