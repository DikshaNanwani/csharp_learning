﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diksha_getset_Aug12
{
    class Library
    {
        int libid = 0;
        string libname = string.Empty;
        string libadd = string.Empty;
        string liblandmark = string.Empty;

        public int Lib_id
        {
            get { return libid; }
            set { libid = value; }
        }

        public string Lib_Name
        {
            get { return libname; }
            set { libname = value; }

        }

        public string Lib_Add
        {
            get { return libadd; }
            set { libadd = value; }

        }

        public Library(int libid, string libname, string libadd)
        {
            libid = Lib_id;
            libname = Lib_Name;
            libadd = Lib_Add;
        }
    }
}


