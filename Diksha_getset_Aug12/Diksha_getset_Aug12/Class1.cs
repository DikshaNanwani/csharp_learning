﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diksha_getset_Aug12
{

    class Employee
    {
        int empid = 0;
        string empname = string.Empty;
        string empadd = string.Empty;

        public int E_id
        {
            get { return empid; }
            set { empid = value; }
        }

        public string Emp_Name
        {
            get { return empname; }
            set { empname = value; }

        }

        public string Emp_Add
        {
            get { return empadd; }
            set { empadd = value; }

        }

        public Employee(int empid,string empname,string empadd)
        {
            empid = E_id;
            empname = Emp_Name;
            empadd = Emp_Add;
        }
    }
}
