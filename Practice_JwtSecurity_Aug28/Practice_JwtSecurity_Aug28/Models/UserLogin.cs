﻿namespace Practice_JwtSecurity_Aug28.Models
{
    public class UserLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
