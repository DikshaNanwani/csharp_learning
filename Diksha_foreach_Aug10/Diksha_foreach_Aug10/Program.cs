﻿using System;

namespace Diksha_foreach_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3, 4, 5 };

            foreach(int item in arr)
            {
                Console.WriteLine(item);
            }
        }
    }
}
